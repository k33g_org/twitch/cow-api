# 🐮 Cow API: Hello

```javascript
fastify.get('/api/hello', async function (req, res) {
  const getRandomNumberBetween = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
  const randomNumber = getRandomNumberBetween(0,50);

  res
    .header('Content-Type', 'application/json; charset=utf-8')
    .send({message: "Hello World", result: randomNumber});
});
```